#!/usr/bin/env python3

from __future__ import print_function
import socket
import random
import datetime

TCP_IP = '127.0.0.1'
TCP_PORT = 5555
BUFFER_SIZE = 1024
DELIMITER = '\n'.encode()

# Configure socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)
print("Listening on \t\t {} {}".format(TCP_IP,TCP_PORT))

# Listen to traffic on socket
while True:
  conn, addr = s.accept()
  print("Connection from address: {}".format(addr))
  data=b''
  while True:
    packet = conn.recv(BUFFER_SIZE)
    if not packet:
        conn.close()
        print("Connection closed\n\n\n")
        break
    else:
        data += packet
    while data:
      # Process data until input buffer contains no more commands
      if DELIMITER in data:
        comm_data = data[0:data.find(DELIMITER)+1].decode()
        data = data[data.find(DELIMITER)+1:]
      else:
        break
      print("Received data: {}".format(repr(comm_data)))
      try:
        comm_data.rstrip()
        if "RAND" in comm_data:
            response = str(random.uniform(0.0,100.0))
        elif "TIME" in comm_data:
          response = str(datetime.datetime.now())
        else:
          raise SyntaxError("Invalid syntax")
      except Exception as e:
        print(e)
        print("Invalid request: {}".format(comm_data))
        response = None
      if response:
        response += DELIMITER.decode()
        conn.send(response.encode())
        print("Sent response (request --> reply): {} --> {}".format(repr(comm_data),repr(response)))
